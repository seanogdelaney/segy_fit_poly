# Command Line Usage: python scriptName input.sgy output.sgy polynomialOrder

# Modules: numpy for maths, sys for command line arguments, segyio for segy
import numpy as np
import sys, segyio

# The user provides the order of the desired polynomial fit on the command line
order = int(sys.argv[3]) # Convert text (string) to an integer, e.g. '3' to 3

# The user provides the input segy on the command line, which we open
# Assume the file layout looks like data[ilines, xlines, offsets, samples]
with segyio.open(sys.argv[1]) as f:

    # Prepare result array. Just like data, offsets replaced by coefficients
    fit = np.ones([f.ilines.size, f.xlines.size, order + 1, f.samples.size], \
        dtype='float32') # 4-byte samples for segy. 'doubles' would be wasteful

    # Loop over the offset gathers (there's one for every iline and xline)
    for il in range(f.ilines.size):
        for xl in range(f.xlines.size):
            # Read gather from file. segyio.gather returns an array 'generator'
            # numpy.polyfit needs a genuine numpy array, so we ensure that
            gather = np.array(f.gather[f.ilines[il], f.xlines[xl], :])
            # Calculate the coefficients. Isn't numpy lovely?
            fit[il, xl, :, :] = np.polyfit(f.offsets, gather, order)

# Write the result array to disk. 
# Headers will be wrong. Could fix here, or leave to the user's fancy software
segyio.tools.from_array4D(sys.argv[2], fit)
