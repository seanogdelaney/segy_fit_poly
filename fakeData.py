# Command line usage: python scriptName
# Make a segy file with simple cubic polynomial AVO (coefficients 3, 4, 5, 6)

import numpy as np
import segyio

# Our AVO: offset dimension of output will have this polynomial behaviour
p = np.array([3*x**3 + 4*x**2 + 5*x + 6 for x in range(1,13)]) # 12 values
# Replicate p with constant behaviour in the other 3 dimensions
a = np.kron(np.ones([10, 11, 1, 13]), p[None, None, :, None])

# Write the resulting [10, 11, 12, 13] array to a segy file
# By default, the offset headers will be 1...12, matching the range() above
segyio.tools.from_array4D('fakeData.sgy', a)
