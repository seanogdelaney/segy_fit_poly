Welcome to git, hosted on bitbucket.org in this case.

Git is a distributed software version control system.
It allows you to track your changes in a coding project.
It also allows you to collaborate with other programmers on the same project.
It provides tools to allow you to work independently, but still make a cohesive product.

I strongly recommend beginners take a brief git tutorial, then learn as they go.

This project is a simple python script to do some maths with segy data.
An extra script is provided to make testing data.